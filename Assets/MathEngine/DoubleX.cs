﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathEngine
{
    public class Double2
    {
        public double x, y;

        public Double2()
        {
            x = y = 0;
        }

        public Double2(double ax, double ay)
        {
            x = ax;
            y = ay;
        }

        public override string ToString()
        {
            return x + " " + y;
        }
    }

    public class Double3
    {
        public double x, y, z;

        public Double3()
        {
            x = y = z = 0;
        }

        public Double3(double ax, double ay, double az)
        {
            x = ax;
            y = ay;
            z = az;
        }

        public override string ToString()
        {
            return x + " " + y + " " + z;
        }

        public string ToString(char separator)
        {
            return x + separator + " " + y + separator + " " + z;
        }
    }
}
