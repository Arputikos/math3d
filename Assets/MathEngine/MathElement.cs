﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathEngine
{
    public enum MathElementType
    {
        Default,
        Constant,
        Argument,
        Parameter,
        Function,
        BinOperator,
        SharedConstant       //for use in MathEval.EvaluateMatrix only, when there is a shared constant
    }

    /// <summary>
    /// A mathematical element - a constant, argument, parameter, operator or a function.
    /// You put it on a stack.
    /// </summary>
    public class MathElement
    {
        public MathElementType ElementType;
        public double Value;


        /// <summary>
        /// Gets a value indicating whether this instance is neither an operator, nor a fuction.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is numeric; otherwise, <c>false</c>.
        /// </value>
        public bool IsNumeric
        {
            get {
                return (ElementType != MathElementType.BinOperator
                        && ElementType != MathElementType.Function);
            }
        }

        public MathElement()
        {
            ElementType = MathElementType.Default;
            Value = 0;
        }

        public MathElement(MathElementType elementType, double value)
        {
            ElementType = elementType;
            Value = value;
        }

        public MathElement(MathElement element)
        {
            ElementType = element.ElementType;
            Value = element.Value;
        }

        public override string ToString()
        {
            switch (ElementType)
            {
                case MathElementType.Argument:
                    if (Value == 1) return "x";
                    else if (Value == 2) return "y";
                    else if (Value == 3) return "t";
                    else return "?";
                case MathElementType.BinOperator:
                    var l1 = Constants.OperatorMap[this];
                    if (l1.Count > 0) return l1[0];
                    else return "?" + Value + "O?";
                case MathElementType.SharedConstant:
                case MathElementType.Constant:
                    var l2 = Constants.ConstantsMap[this];
                    if (l2.Count > 0) return l2[0];
                    else return Value.ToString();
                case MathElementType.Function:
                    var l3 = Constants.FunctionMap[this];
                    if (l3.Count > 0) return l3[0];
                    else return "?" + Value + "F?";
                case MathElementType.Parameter:
                    return "par" + (int)Value;
                    //return parameters[(int)Value][0] + " ";
                default:
                    return "<ERROR>";
            }
        }
    }

}
