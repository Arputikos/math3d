﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;

namespace MathEngine
{
    /// <summary>
    /// Class for parsing math expressions.
    /// </summary>
    public class MathParser
    {
        public List<MathElement> ElementStack;
        public int Count
        {
            get
            {
                if (ElementStack != null) return ElementStack.Count;
                else return 0;
            }
        }

        private BiDictionary<string, int> parameters;

        public MathParser()
        {
            ElementStack = new List<MathElement>();
            parameters = new BiDictionary<string, int>();
        }

        /// <summary>
        /// Checks if the stack is correct
        /// </summary>
        /// <returns></returns>
        public bool CheckStack()
        {
            var st = new Stack<MathElement>();

            foreach (var me in ElementStack)
            {
                switch (me.ElementType)
                {
                    case MathElementType.Argument:
                    case MathElementType.Constant:
                    case MathElementType.Parameter:
                        st.Push(me);
                        break;
                    case MathElementType.BinOperator:
                        if (!(st.Count > 1
                            && st.Pop().IsNumeric
                            && st.Pop().IsNumeric)) return false;
                        else st.Push(Constants.e);
                        break;
                    case MathElementType.Function:
                        if (!(st.Count > 0
                            && st.Pop().IsNumeric)) return false;
                        else st.Push(Constants.e);
                        break;
                    default:
                        return false;
                }
            }
            return (st.Count == 1 && st.Pop().IsNumeric);
        }

        /// <summary>
        /// Reads a math expr in infix notation
        /// </summary>
        /// <param name="input">String to process</param>
        /// <returns>Element count</returns>
        /// <exception cref="Exception">
        /// Incorrect bracket expression
        /// </exception>
        public int ReadInfix(string input)
        {
            input = input.ToLower();
            ElementStack = new List<MathElement>();
            parameters = new BiDictionary<string, int>();
            var st = new Stack<MathElement>();

            string s = input.Trim();
            Regex zeroMinus = new Regex(@"\(\s*\-");    // (-x+2) -> (0-x+2)
            s = zeroMinus.Replace(s, "( 0 - ");
            zeroMinus = new Regex(@"^\s*\-");           // ^-sinx -> ^0-sinx
            s = zeroMinus.Replace(s, "0 - ");
            Regex doppelOperator = new Regex(@"\+\s*\-");   //+ - -> -
            s = doppelOperator.Replace(s, " - ");
            doppelOperator = new Regex(@"\+\s*\+");   //+ + -> +
            s = doppelOperator.Replace(s, " + ");
            doppelOperator = new Regex(@"\*\s*\*");   //* * -> *
            s = doppelOperator.Replace(s, " * ");
            doppelOperator = new Regex(@"\-\s*\-");   //- - -> +
            s = doppelOperator.Replace(s, " + ");
            doppelOperator = new Regex(@"\s+");     //multiple spaces & other whitespace -> single
            s = doppelOperator.Replace(s, " ");
            s = s.Trim();


            MathElement prev = null;
            while (!string.IsNullOrEmpty(s))
            {
                try
                {
                    if (s[0] == '(')
                    {
                        if (prev != null &&
                            (prev.ElementType == MathElementType.Argument ||
                            prev.ElementType == MathElementType.Constant ||
                            prev.ElementType == MathElementType.Parameter ||
                            prev.Value == 101))     //rBracket
                        {

                            while (st.Count > 0 && getPriority(st.Peek()) >= 3)
                                ElementStack.Add(st.Pop());
                            st.Push(new MathElement(Constants.multiply));
                        }
                        st.Push(Constants.lBracket);
                        s = s.Substring(1).Trim();
                        prev = new MathElement(Constants.lBracket);
                    }
                    else if (s[0] == ')')
                    {
                        bool lB = false;
                        while (!lB && st.Count > 0)
                        {
                            var top = st.Pop();
                            if (!(top.ElementType == MathElementType.BinOperator && top.Value == 100)) ElementStack.Add(top);
                            else lB = true;
                        }
                        if (!lB) throw new Exception("Incorrect bracket expression");
                        s = s.Substring(1).Trim();
                        prev = new MathElement(Constants.rBracket);
                    }
                    else
                    {
                        MathElement me = getNextElement(ref s);
                        if (me.ElementType == MathElementType.Argument
                            || me.ElementType == MathElementType.Constant
                            || me.ElementType == MathElementType.Parameter)
                        {
                            if (prev != null &&
                                (prev.ElementType == MathElementType.Argument ||
                                prev.ElementType == MathElementType.Constant ||
                                prev.ElementType == MathElementType.Parameter ||
                                prev.Value == 101))     //rBracket
                            {

                                while (st.Count > 0 && getPriority(st.Peek()) >= 3)
                                    ElementStack.Add(st.Pop());
                                st.Push(new MathElement(Constants.multiply));
                            }
                            ElementStack.Add(me);
                        }
                        else if (me.ElementType == MathElementType.Default) break;
                        else
                        {
                            int p = getPriority(me);
                            while (st.Count > 0 && getPriority(st.Peek()) >= p && p != 5)
                                ElementStack.Add(st.Pop());
                            if(p == 5 && prev != null &&
                                (prev.ElementType == MathElementType.Argument ||
                                prev.ElementType == MathElementType.Constant ||
                                prev.ElementType == MathElementType.Parameter ||
                                prev.Value == 101))     //rBracket
                            {
                                while (st.Count > 0 && getPriority(st.Peek()) >= 3)
                                    ElementStack.Add(st.Pop());
                                st.Push(new MathElement(Constants.multiply));
                            }
                            st.Push(me);
                        }
                        prev = me;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " at char " + (input.Length - s.Length));
                }
            }
            foreach (var me in st)
            {
                if (me.ElementType == MathElementType.BinOperator && (me.Value == 100 || me.Value == 101))
                    throw new Exception("Incorrect bracket expression.");
                else ElementStack.Add(me);
            }

            return Count;
        }

        /// <summary>
        /// Generates postfix notation from stack
        /// </summary>
        /// <returns>Postfix notation string</returns>
        public string WritePostfix()
        {
            string s = "";

            foreach (var me in ElementStack)
            {
                string x = me.ToString();
                if (x.StartsWith("par"))
                    x = parameters[(int)me.Value][0];
                s += x + " ";
            }

            return s.Trim();
        }

        /// <summary>
        /// Reads a math expr in postfix notation
        /// </summary>
        /// <param name="input">String to process</param>
        /// <returns>Element count</returns>
        /// <exception cref="Exception"></exception>
        public int ReadPostfix(string input)
        {
            input = input.ToLower();
            ElementStack = new List<MathElement>();
            parameters = new BiDictionary<string, int>();

            string s = input.Trim();
            while (!string.IsNullOrEmpty(s))
            {
                try
                {
                    MathElement me = getNextElement(ref s);
                    if (me.ElementType != MathElementType.Default)
                        ElementStack.Add(me);
                    else break;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " at char " + (input.Length - s.Length));
                }
            }

            return Count;
        }

        /// <summary>
        /// Gets the next MathElement in specified string. Removes the element from that string.
        /// </summary>
        /// <param name="s">The string to parse.</param>
        /// <returns>First found MathElement.</returns>
        /// <exception cref="Exception">
        /// Unexpected dot in floating-point number
        /// or
        /// Not a number: " + buf.Trim()
        /// or
        /// Unrecognized identifier: '" + oldBuf + "'
        /// </exception>
        private MathElement getNextElement(ref string s)
        {
            s = s.Trim();
            MathElement me = new MathElement();
            if (s.Length == 0) return me;

            string buf = "";
            if ((s[0] >= '0' && s[0] <= '9') || s[0] == '.' || s[0] == ',')        //liczba
            {
                bool kropka = s[0] == '.';
                buf += s[0];
                for (int i = 1; i < s.Length; i++)
                {
                    if (s[i] >= '0' && s[i] <= '9')
                        buf += s[i];
                    else if (s[i] == '.' || s[i] == ',')
                    {
                        if (!kropka)
                        {
                            buf += s[i];
                            kropka = true;
                        }
                        else throw new Exception("Unexpected dot in floating-point number");
                    }
                    else break;
                }
                buf = buf.Replace('.', ',');
                double t = double.NaN;
                double.TryParse(buf.Trim(), System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CreateSpecificCulture("pl-PL"), out t);
                if (double.IsNaN(t))
                    throw new Exception("Not a number: " + buf.Trim());
                else me = new MathElement(MathElementType.Constant, t);
            }
            else    //operator/funkcja/stała/argument/parametr
            {
                buf += s[0];
                for (int i = 1; i < s.Length; i++)
                {
                    if (s[i] > ' ')     //poniżej spacji są same ascii syfy
                        buf += s[i];
                    else break;
                }
                buf = buf.Trim();
                string oldBuf = buf;
                bool done = false;
                for (; buf.Length > 0; buf = buf.Substring(0, buf.Length - 1))
                {
                    buf.Trim();
                    if (!done)
                    {
                        var l = Constants.FunctionMap[buf];
                        if (l.Count > 0)
                        {
                            me = l[0];
                            done = true;
                        }
                    }
                    if (!done)
                    {
                        var l = Constants.OperatorMap[buf];
                        if (l.Count > 0)
                        {
                            me = l[0];
                            done = true;
                        }
                    }
                    if (!done)
                    {
                        var l = Constants.ConstantsMap[buf];
                        if (l.Count > 0)
                        {
                            me = l[0];
                            done = true;
                        }
                    }
                    if (!done && buf.Length == 1)
                    {

                        if (buf[0] == 'x') me = new MathElement(MathElementType.Argument, 1);
                        else if (buf[0] == 'y') me = new MathElement(MathElementType.Argument, 2);
                        else if (buf[0] == 't') me = new MathElement(MathElementType.Argument, 3);
                        else if (buf[0] >= 'a' && buf[0] <= 'z')
                        {
                            var l = parameters[buf];
                            if (l.Count > 0) me = new MathElement(MathElementType.Parameter, l[0]);
                            else
                            {
                                parameters.Add(buf, parameters.Count + 1);
                                me = new MathElement(MathElementType.Parameter, parameters.Count);
                            }
                        }
                        else throw new Exception("Unrecognized identifier: '" + oldBuf + "'");
                        done = true;
                    }
                    if (done) break;
                }

            }
            s = s.Substring(buf.Length).Trim();

            return me;
        }

        /// <summary>
        /// Gets the priority for specified MathElement
        /// </summary>
        /// <param name="me">MathElement to determine the priority of.</param>
        /// <returns>Priority</returns>
        private int getPriority(MathElement me)
        {
            if (me.ElementType == MathElementType.Function) return 5;
            if (me.ElementType == MathElementType.BinOperator)
            {
                if (me.Value > 99)
                    return 0;
                switch ((Oe)(int)me.Value)
                {
                    case Oe.min:
                    case Oe.max:
                        return 1;
                    case Oe.add:
                    case Oe.subtract:
                        return 2;
                    case Oe.multiply:
                    case Oe.divide:
                    case Oe.mod:
                        return 3;
                    case Oe.pow:
                    default:
                        return 4;
                }
            }
            return 0;
        }
    }
}
