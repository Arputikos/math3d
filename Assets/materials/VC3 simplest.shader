﻿Shader "Custom/VC3 simplest" {
     Properties {
         _Color ("Color", Color) = (1,1,1,1)
     }
     SubShader {
         Tags { "RenderType"="Opaque" }
         LOD 200
         
         CGPROGRAM
         #pragma surface surf NoLighting vertex:vert
         #pragma target 3.0
         struct Input {
             float3 vertexColor; // Vertex color stored here by vert() method
         };
         
         struct v2f {
           float4 pos : SV_POSITION;
           fixed4 color : COLOR;
         };
 
         void vert (inout appdata_full v, out Input o)
         {
             UNITY_INITIALIZE_OUTPUT(Input,o);
             o.vertexColor = v.color; // Save the Vertex Color in the Input for the surf() method
         }
         fixed4 _Color;
 
		 fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
		 {
			 fixed4 c;
			 c.rgb = s.Albedo;
			 return c;
		 }

         void surf (Input IN, inout SurfaceOutput o) 
         {
             // Albedo comes from by color
             fixed4 c = _Color;
             o.Albedo = /*c.rgb **/ IN.vertexColor; // Combine normal color with the vertex color
             //o.Alpha = c.a;
         }
         ENDCG
     } 
     FallBack "Diffuse"
 }