﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class GridController : MonoBehaviour {

    public LineRenderer ProtoLine;
    public LineRenderer AxisX, AxisY, AxisZ;
    public InputField AxisWidthInput, GridWidthInput;
    public InputField GridXInput, GridYInput, GridZInput;
    public Toggle ShowXYToggle, ShowYZToggle, ShowZXToggle;
    public GameObject text3Dprefab;
    public Dropdown dropdMode;

    private List<LineRenderer> gridXY = new List<LineRenderer>();
    private List<LineRenderer> gridYX = new List<LineRenderer>();
    private List<LineRenderer> gridYZ = new List<LineRenderer>();
    private List<LineRenderer> gridZY = new List<LineRenderer>();
    private List<LineRenderer> gridXZ = new List<LineRenderer>();
    private List<LineRenderer> gridZX = new List<LineRenderer>();

    private GameObject[] gridLabels;

    private List<GameObject> labels = new List<GameObject>();

    public MeshGenerator meshGenerator;

    public enum GridMode {
        WALLS,
        CENTERED
    }
    private GridMode gmode = GridMode.WALLS;

    public bool showXY = true;
    public bool showYZ = true;
    public bool showXZ = true;

    private float prevX = 0;
    private float prevY = 0;
    private float prevZ = 0;

    private float axisWidth = 1;
    private float gridWidth = 0.1f;     

    private int CountXY = 1;
    private int CountYX = 1;
    private int CountYZ = 1;
    private int CountZY = 1;
    private int CountXZ = 1;
    private int CountZX = 1;


    public void SetGridMode(int m)
    {
        gmode = (GridMode)m;
        RedrawLines();
        RedrawLabels();
    }

    public void SomeGridPropertyChanged()
    {
        float.TryParse(AxisWidthInput.text, out axisWidth);
        axisWidth = Mathf.Clamp(axisWidth, 0.1f, 2);

        float.TryParse(GridWidthInput.text, out gridWidth);
        gridWidth = Mathf.Clamp(gridWidth, 0.1f, 2);

        int.TryParse(GridXInput.text, out CountXY);
        CountXY = Clamp(CountXY, 1, 20);
        CountXZ = CountXY;

        int.TryParse(GridYInput.text, out CountYZ);
        CountYZ = Clamp(CountYZ, 1, 20);
        CountYX = CountYZ;

        int.TryParse(GridZInput.text, out CountZY);
        CountZY = Clamp(CountZY, 1, 20);
        CountZX = CountZY;
        
        showXY = ShowXYToggle.isOn;
        showYZ = ShowYZToggle.isOn;
        showXZ = ShowZXToggle.isOn;

        RedrawLines();
        RedrawLabels();
        setUI();
        
    }
    public void GridLabelUpdate()
    {
        RedrawLabels();
    }

    private void setUI()
    {
        AxisWidthInput.text = axisWidth.ToString();
        GridWidthInput.text = gridWidth.ToString();
        GridXInput.text = CountXY.ToString();
        GridYInput.text = CountYZ.ToString();
        GridZInput.text = CountZX.ToString();
        dropdMode.value = (int)gmode;
    }

    void Start () {
        CreateLabels();
        RedrawLines();
        RedrawLabels();
        setUI();
    }

    void Update()
    {
        if (gmode == GridMode.WALLS)
        {
            Vector3 pos = Camera.main.gameObject.transform.position;
            if (prevX * pos.x < 0 || prevY * pos.y < 0 || prevZ * pos.z < 0)  //position sign changed, redraw
            {
                RedrawLines();
                RedrawLabels();
            }

            prevX = pos.x;
            prevY = pos.y;
            prevZ = pos.z;
        }

        //labels - billboarding, look at camera
        for (int i = 0; i < 3; i++)
        {
            gridLabels[i].transform.rotation = Camera.main.transform.rotation;
        }
        foreach (var v in labels)
            v.transform.rotation = Camera.main.transform.rotation;
    }

    private void CreateLabels()
    {
        int gridLabelSize = 100;
        float margin = gridLabelSize / 50.0f;

        gridLabels = new GameObject[3];
        for (int i = 0; i < 3; i++)
        {
            gridLabels[i] = Instantiate(text3Dprefab);
            gridLabels[i].GetComponent<TextMesh>().fontSize = gridLabelSize;
        }

        gridLabels[0].GetComponent<TextMesh>().text = "X";
        gridLabels[1].GetComponent<TextMesh>().text = "Y";
        gridLabels[2].GetComponent<TextMesh>().text = "Z";
        
    }
    private void RedrawLines()
    {
        float oXY = 0;
        float oYZ = 0;
        float oXZ = 0;
        if (gmode == GridMode.WALLS)
        { 
            if (Camera.main.gameObject.transform.position.y > 0) oXY = -20;
            else oXY = 20;
            if (Camera.main.gameObject.transform.position.x > 0) oYZ = -20;
            else oYZ = 20;
            if (Camera.main.gameObject.transform.position.z > 0) oXZ = -20;
            else oXZ = 20;
        }
        AxisX.SetPositions(new Vector3[] { new Vector3(-20, oXY, oXZ), new Vector3(18.9f, oXY, oXZ), new Vector3(19, oXY, oXZ), new Vector3(20, oXY, oXZ) });
        AxisY.SetPositions(new Vector3[] { new Vector3(oYZ, -20, oXZ), new Vector3(oYZ, 18.9f, oXZ), new Vector3(oYZ, 19, oXZ), new Vector3(oYZ, 20, oXZ) });
        AxisZ.SetPositions(new Vector3[] { new Vector3(oYZ, oXY, -20), new Vector3(oYZ, oXY, 18.9f), new Vector3(oYZ, oXY, 19), new Vector3(oYZ, oXY, 20) });
        AxisX.widthMultiplier = axisWidth;
        AxisY.widthMultiplier = axisWidth;
        AxisZ.widthMultiplier = axisWidth;

        foreach (var x in gridXY) DestroyImmediate(x.gameObject);
        gridXY = new List<LineRenderer>();
        foreach (var x in gridYX) DestroyImmediate(x.gameObject);
        gridYX = new List<LineRenderer>();

        foreach (var x in gridYZ) DestroyImmediate(x.gameObject);
        gridYZ = new List<LineRenderer>();
        foreach (var x in gridZY) DestroyImmediate(x.gameObject);
        gridZY = new List<LineRenderer>();

        foreach (var x in gridXZ) DestroyImmediate(x.gameObject);
        gridXZ = new List<LineRenderer>();
        foreach (var x in gridZX) DestroyImmediate(x.gameObject);
        gridZX = new List<LineRenderer>();

        if (showXY)
        {
            for (int i = 0; i < CountXY * 2 + 1; i++)
            {
                gridXY.Add(Instantiate(ProtoLine));
                var g = gridXY[i];
                g.SetPositions(new Vector3[] { new Vector3(-20.0f + 20.0f * i / CountXY, oXY, -20), new Vector3(-20.0f + 20.0f * i / CountXY, oXY, 20) });
                g.widthMultiplier = gridWidth;
                g.gameObject.SetActive(true);
            }
            for (int i = 0; i < CountYX * 2 + 1; i++)
            {
                gridYX.Add(Instantiate(ProtoLine));
                var g = gridYX[i];
                g.SetPositions(new Vector3[] { new Vector3(-20, oXY, -20.0f + 20.0f * i / CountYX), new Vector3(20, oXY, -20.0f + 20.0f * i / CountYX) });
                g.widthMultiplier = gridWidth;
                g.gameObject.SetActive(true);
            }
        }
        if (showYZ)
        {
            for (int i = 0; i < CountYZ * 2 + 1; i++)
            {
                gridYZ.Add(Instantiate(ProtoLine));
                var g = gridYZ[i];
                g.SetPositions(new Vector3[] { new Vector3( oYZ, -20, -20.0f + 20.0f * i / CountYZ), new Vector3(oYZ, 20, -20.0f + 20.0f * i / CountYZ) });
                g.widthMultiplier = gridWidth;
                g.gameObject.SetActive(true);
            }
            for (int i = 0; i < CountZY * 2 + 1; i++)
            {
                gridZY.Add(Instantiate(ProtoLine));
                var g = gridZY[i];
                g.SetPositions(new Vector3[] { new Vector3(oYZ, -20.0f + 20.0f * i / CountZY, -20), new Vector3(oYZ, -20.0f + 20.0f * i / CountZY, 20) });
                g.widthMultiplier = gridWidth;
                g.gameObject.SetActive(true);
            }
        }
        
        if (showXZ)
        {
            for (int i = 0; i < CountXZ * 2 + 1; i++)
            {
                gridXZ.Add(Instantiate(ProtoLine));
                var g = gridXZ[i];
                g.SetPositions(new Vector3[] { new Vector3(-20.0f + 20.0f * i / CountXZ, -20, oXZ), new Vector3(-20.0f + 20.0f * i / CountXZ, 20, oXZ) });
                g.widthMultiplier = gridWidth;
                g.gameObject.SetActive(true);
            }
            for (int i = 0; i < CountZX * 2 + 1; i++)
            {
                gridZX.Add(Instantiate(ProtoLine));
                var g = gridZX[i];
                g.SetPositions(new Vector3[] { new Vector3(-20, -20.0f + 20.0f * i / CountZX, oXZ), new Vector3(20, -20.0f + 20.0f * i / CountZX, oXZ) });
                g.widthMultiplier = gridWidth;
                g.gameObject.SetActive(true);
            }
        }

    }
    private void RedrawLabels()
    {
        //-----------------------grid-------------------
        float gridMargin = 7.0f;
        float oXY = 0;
        float oYZ = 0;
        float oXZ = 0;
        if (gmode == GridMode.WALLS)
        {
            if (Camera.main.gameObject.transform.position.y > 0) oXY = -20;
            else oXY = 20;
            if (Camera.main.gameObject.transform.position.x > 0) oYZ = -20;
            else oYZ = 20;
            if (Camera.main.gameObject.transform.position.z > 0) oXZ = -20;
            else oXZ = 20;
        }
        else
        {
            oXY = oXZ = oYZ = 0;
        }
        //with swap Y-Z because of unity coord
        gridLabels[0].transform.position = new Vector3(-Mathf.Sign(oYZ)*(20 + gridMargin), oXY, oXZ);
        gridLabels[1].transform.position = new Vector3(oYZ, oXY, -Mathf.Sign(oXZ)*(20 + gridMargin));
        gridLabels[2].transform.position = new Vector3(oYZ, -Mathf.Sign(oXY)*(20 + gridMargin), oXZ);

        //-----------------labels-------------------
        int labelSize = 70;
        float margin = 3.0f;

        foreach (var x in labels) DestroyImmediate(x.gameObject);
        labels = new List<GameObject>();

        if (gmode == GridMode.WALLS)
        {
            if (Camera.main.gameObject.transform.position.y > 0) oXY = -20;
            else oXY = 20;
            if (Camera.main.gameObject.transform.position.x > 0) oYZ = -20;
            else oYZ = 20;
            if (Camera.main.gameObject.transform.position.z > 0) oXZ = -20;
            else oXZ = 20;
        }
        else
        {
            oXY = 0.0f;
            if (Camera.main.gameObject.transform.position.x > 0) oYZ = -20;
            else oYZ = 20;
            if (Camera.main.gameObject.transform.position.z > 0) oXZ = -20;
            else oXZ = 20;
        }

        Vector3 min, max;
        min = meshGenerator.getPlotBoundsMin();
        max = meshGenerator.getPlotBoundsMax();
        float dX = (max.x - min.x) / (CountXZ * 2);
        float dY = (max.y - min.y) / (CountYZ * 2);
        float dZ = (max.z - min.z) / (CountZX * 2);

        int[] digitsAfterPoint = new int[3];//x y z
        //calculating precise
        //1-0.1 zakres : 2 cyfry, 10-1 1 cyfra po ,
        if (max.x - min.x != 0)
            digitsAfterPoint[0] = Mathf.FloorToInt(Mathf.Log10(1.0f / (max.x - min.x) * 10) + 1);
        else digitsAfterPoint[0] = 4;
        if (max.y - min.y != 0)
            digitsAfterPoint[1] = Mathf.FloorToInt(Mathf.Log10(1.0f/(max.y - min.y)*10) + 1);
        else digitsAfterPoint[1] = 4;
        if (max.z - min.z != 0)
            digitsAfterPoint[2] = Mathf.FloorToInt(Mathf.Log10(1.0f/(max.z - min.z)*10) + 1);
        else digitsAfterPoint[2] = 4;
       

        if (digitsAfterPoint[0] < 0) digitsAfterPoint[0] = 0;
        if (digitsAfterPoint[1] < 0) digitsAfterPoint[1] = 0;
        if (digitsAfterPoint[2] < 0) digitsAfterPoint[2] = 0;
        //float avoidTextOverlapMarginMax = 2.0f;
        if (showXY)
        {
            for (int i = 0; i < CountXY * 2 + 1; i++)
            {
                labels.Add(Instantiate(text3Dprefab));
                var l = labels[labels.Count - 1];
                l.GetComponent<TextMesh>().fontSize = labelSize;

                string format = "{0:0";
               if (digitsAfterPoint[0] > 0) format += ".";
               for (int j = 0; j < digitsAfterPoint[0]; j++) format += "0";
               format += "}";

                l.GetComponent<TextMesh>().text = string.Format(format, min.x + dX * i);
                l.transform.position = new Vector3(-20.0f + 20.0f * i / CountXY, oXY, -oXZ - Mathf.Sign(oXZ) * margin);
                //if ((showXZ || showYZ) && i == CountXY*2)//first little up to avoid collision with label XY
                //    l.transform.Translate(new Vector3((-avoidTextOverlapMarginMax < -20.0f / CountZX / 2.0f ? -20.0f / CountZX / 2.0f : -avoidTextOverlapMarginMax), 0.0f, 0.0f));
            }
            for (int i = 0; i < CountYZ * 2 + 1; i++)
            {
                labels.Add(Instantiate(text3Dprefab));
                var l = labels[labels.Count - 1];
                l.GetComponent<TextMesh>().fontSize = labelSize;

                string format = "{0:0";
                if (digitsAfterPoint[1] > 0) format += ".";
                for (int j = 0; j < digitsAfterPoint[1]; j++) format += "0";
                format += "}";

                l.GetComponent<TextMesh>().text = string.Format(format, min.y + dY * i);
                l.transform.position = new Vector3(-oYZ - Mathf.Sign(oYZ) * margin, oXY, -20.0f + 20.0f * i / CountYZ);
                //if ((showXZ || showYZ) && i == CountYZ * 2)//first little up to avoid collision with label XY
                //    l.transform.Translate(new Vector3(0.0f, 0.0f, -avoidTextOverlapMarginMax < -20.0f / CountZX / 2.0f ? -20.0f / CountZX / 2.0f : -avoidTextOverlapMarginMax));
            }
        }
        if (showXZ || showYZ)
        {
            int oppositeSide = 1;//1 v -1
            //normally, this is on one wall, XZ
            //but when XZ turned off, show on YZ
            if (showYZ && !showXZ)
                oppositeSide = -1;

            for (int i = 0; i < CountZX * 2 + 1; i++)
            {
                labels.Add(Instantiate(text3Dprefab));
                var l = labels[labels.Count - 1];
                l.GetComponent<TextMesh>().fontSize = labelSize;

                string format = "{0:0";
                if (digitsAfterPoint[2] > 0) format += ".";
                for (int j = 0; j < digitsAfterPoint[2]; j++) format += "0";
                format += "}";
                l.GetComponent<TextMesh>().text = string.Format(format, min.z + dZ * i);
                if (gmode == GridMode.WALLS)
                    l.transform.position = new Vector3(oppositeSide * (-oYZ - Mathf.Sign(oYZ) * margin), -20.0f + 20.0f * i / CountZX, oppositeSide * oXZ);
                else
                    l.transform.position = new Vector3((oppositeSide + 1) / 2 * (-oYZ) - Mathf.Sign(oYZ) * margin, -20.0f + 20.0f * i / CountZX, 0.0f + (oppositeSide - 1) / 2 * oXZ);
                //f(showXY && i==0)//first little up to avoid collision with label XY
                 //   l.transform.Translate(new Vector3(0.0f, 0.0f, avoidTextOverlapMarginMax > 20.0f / CountZX / 2.0f ? 20.0f / CountZX / 2.0f : avoidTextOverlapMarginMax));
            }
        }
    }
    private int Clamp(int x, int min, int max)
    {
        if (x < min) return min;
        if (x > max) return max;
        return x;
    }

    public GridMode GetMode() {return gmode;}
}
