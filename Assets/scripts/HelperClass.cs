﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelperClass : MonoBehaviour {

    public GameObject outputTextInfoObj;

    public const bool DEBUG_MODE = false;//-----------CZY TRYB DEBUG APLIKACJI CAŁEJ--------------

    void Start()
    {
        outputTextInfoObj.SetActive(DEBUG_MODE);
    }

    //własny szybki clamp
    public static float Clamp(float val, float min, float max)
    {
        if (val < min)
            val = min;
        else if (val > max)
            val = max;

        return val;
    }

    public static void error(string text)
    {
        if (!DEBUG_MODE) return;//można też scrashować ale spróbujmy
        Debug.LogError(text);
    }
    public static void log(string text)
    {
        if (!DEBUG_MODE) return;
        Debug.LogError(text);
    }
}
