﻿using UnityEngine;
using System.Collections;
using MathEngine;

//TODO
//camera look Attribute CNETER of mesh
public class MainController : MonoBehaviour {

    const float MOUSE_SCROLL_SPEED = 2;

    Vector3 cameraOrbitCenter;
    float rotateSpeed;

    float deltaTimeFPS = 0.0f;

	// Use this for initialization
	void Start () {
        cameraOrbitCenter = new Vector3(0.0f, 0.0f, 0.0f);
        rotateSpeed = 10.0f;
#if UNITY_ANDROID
        rotateSpeed = 1.0f;
#endif

        Camera.main.transform.LookAt(cameraOrbitCenter);
        Camera.main.transform.RotateAround(cameraOrbitCenter, Vector3.up, 25.0f);
        Camera.main.transform.RotateAround(cameraOrbitCenter, Camera.main.transform.right, 35.0f);

        Constants.Initialise();     //initialise function dictionaries
	}
	
	// Update is called once per frame
	void Update () {
        //FPS
        deltaTimeFPS += (Time.deltaTime - deltaTimeFPS) * 0.1f;

        /*if (cameraOrbitCenter == null)
        {
            GameObject mesh = GameObject.FindGameObjectWithTag("mesh3D");
            if (!mesh.GetComponent<MeshFilter>())
                return;
            if (mesh == null)
                return;

                cameraOrbitCenter = mesh.transform;
                cameraOrbitCenter.transform.position = cameraOrbitCenter.transform.position - mesh.GetComponent<MeshRenderer>().bounds.size / 2.0f;
        }*/
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        bool android = false;
        #if UNITY_ANDROID
                android = true;
        #endif
        if (Input.GetMouseButton(1) || android)
        {
            Camera.main.transform.LookAt(cameraOrbitCenter);
            Camera.main.transform.RotateAround(cameraOrbitCenter, Vector3.up, Input.GetAxis("Mouse X") * rotateSpeed);
            Camera.main.transform.RotateAround(cameraOrbitCenter, Camera.main.transform.right, -Input.GetAxis("Mouse Y") * rotateSpeed);
        }
        if (Camera.main.orthographic)
        {
            float z = Camera.main.orthographicSize;
            z -= MOUSE_SCROLL_SPEED * Input.mouseScrollDelta.y;
            z = HelperClass.Clamp(z, 2, 150);
            Camera.main.orthographicSize = z;
        }
        else
        {
            Vector3 pos = Camera.main.transform.position;
            pos += MOUSE_SCROLL_SPEED * Input.mouseScrollDelta.y * Camera.main.transform.forward;
            Camera.main.transform.position = pos;
        }
        if (Input.GetKeyDown(KeyCode.T))
            CameraSnap(0);
        else if (Input.GetKeyDown(KeyCode.L))
            CameraSnap(1);
        else if (Input.GetKeyDown(KeyCode.F))
            CameraSnap(2);
	}

    public float GetFPS()
    {
        return 1.0f / deltaTimeFPS;
    }

    public void CameraSnap(int posNum)
    {
        //look from the top
        if(posNum==0)
        {
            //camera top - y
            Camera.main.transform.position = new Vector3(0.0f, 20 + 5, 0.0f);
            Camera.main.transform.eulerAngles = new Vector3(90.0f, -90.0f, 180.0f);
        }
        else if (posNum==1)
        {
            //camera left - z
            Camera.main.transform.position = new Vector3(0.0f, 0.0f, 20 + 5);
            Camera.main.transform.eulerAngles = new Vector3(0.0f, 180.0f, 0.0f);
        }
        else
        {
            //camera front - x
            Camera.main.transform.position = new Vector3(20 + 5, 0.0f, 0.0f);
            Camera.main.transform.eulerAngles = new Vector3(0.0f, -90.0f, 0.0f);
        }
    }
}
