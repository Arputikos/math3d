//działa? chyba...
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using MathEngine;
using System;
using UnityStandardAssets.ImageEffects;
using System.Threading;
using System.Collections.Generic;

//using UnityEditor;

/* MANUAL
 * max vertexów jest 65 000 w unity dlatego tylko tyle trójkątów zrobi około 150 000    //może podzielić to na 4 meshe? #starosta
 * uwaga pokazywanie koordynatów bardzo muli
 *      brak obsługi vertexów z NaN : TODO
 *      dla dużych zakresów dużo trójkątów, gęstrość za duża : TODO
 * threads to jest max threads, można dać więcej niż się ma
 * jeśli jest wierzchołek NaN to zeruje
 * 
 * układ sferyczny==
 * -zamiast x to kąt XY
 * -zamiast y to kąt drugi, między prostą przebijającą XY a pł. XY
 * czyli przycięcie tych wartości przycina te kąty - wycinek sfery - jak się poda za dużo (>2PI)lub ujemne to bierze resztę z dzielenie
 * */
//jak się da w sferycznym clamp lub abs to nie zmienia heightmapy tylko [rzy renderowaniu]

//!uwaga wielkość UI zależy od wysokości the panel'u
public struct sGFXInfo
{
    public int meshX, meshY, verts, tris;
    //public int meshDensity;
    public float fps;
    public string errors;
    public float minZ, maxZ;
    public bool meshUpdated;
};

public enum MESH_COLOR
{
    SINGLE,
    SINGLE_WHITE,
    HEIGHT,
    HEIGHT_WHITE,
    HEIGHT_MAP,
    FANCY
};

public enum RENDER_MODE
{
    SOLID,
    WIREFRAME
};

public enum COORD_SYS
{
    CARTESIAN,
    SPHERICAL,
    CYLINDRICAL
};

public partial class MeshGenerator : MonoBehaviour
{
    private sGFXInfo gfxInfo;
    private MESH_COLOR meshColor;
    private RENDER_MODE renderMode;
    private COORD_SYS coordSystem;
    private SPH_CYL_NEGATIVE sphericalNegative;
    public Material meshMaterialOneSided, meshMaterialTwoSided, meshMaterialWireOne, meshMaterialWireTwo;
    public GameObject canvasUI, canvasUIshow;
    public GameObject Gizmo;
    public LineRenderer HintLineX, HintLineY, HintLineZ;
    public LineRenderer HintLineXY, HintLineYX, HintLineYZ, HintLineZY, HintLineZX, HintLineXZ;
    public GridController gridControl;

    public GameObject pointCoord, pointCoordText, pointCoordTextPanel;

    public GameObject panelUI;

    private int meshX, meshY;
    //private int meshDensity;

    private GameObject meshObj;

    private bool ssao, gizmo, doubleSided, coords;
    private bool UIvisible;

    public MainController mainCtrl;

    private MathParser parser;      //function parser
    private MathEval mev;           //evaluator

    //-----------ranges------------
    //zazwyczaj tak oznacza się w matmie
    private float minX, maxX;       //X - szerokość, dla cyl wysokość
    private float minY, maxY;       //Y - głębokość
    private float zScale;           //Z - wysokość (wartość)
    private int maxVerts;
    private float timeStart, timeEnd, currTime;
    float[] heightMap;
    private Color[] colorMap;
    private const int COLOR_RES = 0x1000000;

    private int threads;

    //pomocnicze
    private bool meshCreated = false;
    private float fpsTimer = 0.0f;

    //optymalizacja - not called everty frame:
    //'wskaźnik' do vertsów updateowany co nowy meesh
    /* verts
    dla cartesian: - aktualne wierzchołki
    dla spherical: - znormalizowane wierzchołki, kula o r = 1, przy każdej klatce tylko skaluje
     */
    Vector3[] verts;
    Vector2[] angles;//only spherical, and cylindrical=only x (height from verts y, angle from angles.x)


    void Start()
    {
        outputTextInfo.text = "";
        verts = null;
        angles = null;
        UpdateGFXInfo(0,0);
        gfxInfo.meshUpdated = false;
        gfxInfo.minZ = 0.0f;
        gfxInfo.maxZ = 0.0f;

        //interpolacja kolorów
        colorMap = new Color[COLOR_RES];
        for (int i=0; i<COLOR_RES; i++)
        {
            float x = (float)i / COLOR_RES;
            x = (1.0f - x) * 0.85f;
            colorMap[i] = Color.HSVToRGB(x, 1.0f, 1.0f);
        }

        Init();
        CreateMesh();

        AssignToUI();
    }
    void Update () {

        //VERY SLOWING DOWN
        if (coords && IsTimeStopped())//ONLY ON PAUSE
        {
            Ray ray;
            RaycastHit hit;

            meshObj.GetComponent<MeshCollider>().sharedMesh = meshObj.GetComponent<MeshFilter>().mesh;//every frame!
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) && hit.collider.name == meshObj.name)
            {
                if (!pointCoord.activeSelf)//pointCoordText.GetComponent<Text>().text == "")
                    pointCoord.SetActive(true);

                //text, zamienione y i z
                string text = "";
                if (coordSystem == COORD_SYS.CARTESIAN)
                {
                    //oblicza prawdziwą wartość (a nie kurna interpolowaną quadami, wykresu)
                    float x = hit.point.x * (maxX - minX) / 40;     //(-20,20) is the graph span in-engine
                    float y = hit.point.z * (maxY - minY) / 40;

                    float z = (float)mev.Evaluate(x, y, currTime);
                    text = ("X = " + x + "\nY = " + y + "\nZ = " + z);

                    float target = 0;
                    if (gridControl.GetMode() == GridController.GridMode.WALLS) target = 20;

                    if (gridControl.showYZ)
                    {
                        HintLineX.gameObject.SetActive(true);
                        HintLineX.SetPositions(new Vector3[] { hit.point,
                            new Vector3(Camera.main.transform.position.x > 0 ? -target : target, hit.point.y, hit.point.z) });
                        HintLineYZ.gameObject.SetActive(true);
                        HintLineYZ.SetPositions(new Vector3[]
                        {
                            new Vector3(Camera.main.transform.position.x > 0 ? -target : target, -20, hit.point.z),
                            new Vector3(Camera.main.transform.position.x > 0 ? -target : target, 20, hit.point.z)
                        });
                        HintLineZY.gameObject.SetActive(true);
                        HintLineZY.SetPositions(new Vector3[]
                        {
                            new Vector3(Camera.main.transform.position.x > 0 ? -target : target, hit.point.y, -20),
                            new Vector3(Camera.main.transform.position.x > 0 ? -target : target, hit.point.y, 20)
                        });
                    }
                    else
                    {
                        HintLineX.gameObject.SetActive(false);
                        HintLineYZ.gameObject.SetActive(false);
                        HintLineZY.gameObject.SetActive(false);
                    }

                    if (gridControl.showXY)
                    {
                        HintLineY.gameObject.SetActive(true);
                        HintLineY.SetPositions(new Vector3[] { hit.point,
                            new Vector3(hit.point.x, Camera.main.transform.position.y > 0 ? -target : target, hit.point.z) });
                        HintLineZX.gameObject.SetActive(true);
                        HintLineZX.SetPositions(new Vector3[]
                        {
                            new Vector3(hit.point.x, Camera.main.transform.position.y > 0 ? -target : target, -20),
                            new Vector3(hit.point.x, Camera.main.transform.position.y > 0 ? -target : target, 20)
                        });
                        HintLineXZ.gameObject.SetActive(true);
                        HintLineXZ.SetPositions(new Vector3[]
                        {
                            new Vector3(-20, Camera.main.transform.position.y > 0 ? -target : target, hit.point.z),
                            new Vector3(20, Camera.main.transform.position.y > 0 ? -target : target, hit.point.z)
                        });
                    }
                    else
                    {
                        HintLineY.gameObject.SetActive(false);
                        HintLineXZ.gameObject.SetActive(false);
                        HintLineZX.gameObject.SetActive(false);
                    }

                    if (gridControl.showXZ)
                    {
                        HintLineZ.gameObject.SetActive(true);
                        HintLineZ.SetPositions(new Vector3[] { hit.point,
                            new Vector3(hit.point.x, hit.point.y, Camera.main.transform.position.z > 0 ? -target : target) });
                        HintLineXY.gameObject.SetActive(true);
                        HintLineXY.SetPositions(new Vector3[]
                        {
                            new Vector3(-20, hit.point.y, Camera.main.transform.position.z > 0 ? -target : target),
                            new Vector3(20, hit.point.y, Camera.main.transform.position.z > 0 ? -target : target)
                        });
                        HintLineYX.gameObject.SetActive(true);
                        HintLineYX.SetPositions(new Vector3[]
                        {
                            new Vector3(hit.point.x, -20, Camera.main.transform.position.z > 0 ? -target : target),
                            new Vector3(hit.point.x, 20, Camera.main.transform.position.z > 0 ? -target : target)
                        });
                    }
                    else
                    {
                        HintLineZ.gameObject.SetActive(false);
                        HintLineXY.gameObject.SetActive(false);
                        HintLineYX.gameObject.SetActive(false);
                    }
                }
                else
                {
                    deactivateAllHelpers();

                    if (coordSystem == COORD_SYS.SPHERICAL)
                    {
                        Vector3 vertex = hit.point.normalized;
                        float a, b;//kąty
                        a = Mathf.Atan2(vertex.z, vertex.x) + Mathf.PI;
                        b = Mathf.Acos(vertex.y);

                        float r = (float)mev.Evaluate(a, b, currTime);
                        text = ("Ѳ = " + Mathf.Round(Mathf.Rad2Deg * a * 100) / 100.0f + "° = " + Mathf.Round(a / Mathf.PI * 100) / 100.0f + "π = " + Mathf.Round(a * 100) / 100.0f + "\n" +
                                "Ф = " + Mathf.Round(Mathf.Rad2Deg * b * 100) / 100.0f + "° = " + Mathf.Round(b / Mathf.PI * 100) / 100.0f + "π = " + Mathf.Round(b * 100) / 100.0f + "\n" +
                                "Radius = " + r);
                    }
                    else if (coordSystem == COORD_SYS.CYLINDRICAL)
                    {
                        Vector3 vertex = hit.point.normalized;
                        float a, z;
                        a = Mathf.Atan2(vertex.z, vertex.x) + Mathf.PI;
                        z = hit.point.y;

                        float r = (float)mev.Evaluate(a, z, currTime);
                        text = ("Ѳ = " + Mathf.Round(Mathf.Rad2Deg * a * 100) / 100.0f + "° = " + Mathf.Round(a / Mathf.PI * 100) / 100.0f + "π = " + Mathf.Round(a * 100) / 100.0f + "\n" +
                                "z = " + z + "\n" +
                                "Radius = " + r);
                    }
                }

                if (mev.ContainsT) text += "\nt =  " + currTime;
                pointCoordText.GetComponent<Text>().text = text;

                //position of panel
                Vector2 pos = pointCoord.transform.position;
                pos.x = Input.mousePosition.x + pointCoordText.GetComponent<RectTransform>().sizeDelta.x * 0.7f;
                pos.y = Input.mousePosition.y - pointCoordText.GetComponent<RectTransform>().sizeDelta.y * 0.7f;
                pointCoord.transform.position = pos;

                Vector2 size = pointCoordText.GetComponent<RectTransform>().sizeDelta;

                pointCoordTextPanel.GetComponent<RectTransform>().sizeDelta = size + new Vector2(10.0f, 10.0f);

            }
            else
            {
                deactivateAllHelpers();
                if (pointCoordText.GetComponent<Text>().text != "")
                {
                    pointCoordText.GetComponent<Text>().text = "";
                    pointCoord.SetActive(false);
                }
            }
        }
        else deactivateAllHelpers();
        //==================-

        if (parser == null)
            ParseFunction();

        //heightmap i color
        gfxInfo.meshUpdated = false;

        if (mev != null && mev.ContainsT && !IsTimeStopped())//jeśli od czasu tylko
        {
            UpdateMesh();
            gridControl.GridLabelUpdate();//update values
        }

        //=============CZAS=========== UWAGA affected by time.timeScale!!!
        if (meshCreated)
        {
            currTime += Time.deltaTime;
            if (currTime > timeEnd && timeStart != timeEnd)//gdy start = end to forever
                currTime = timeStart;
        }
        
        sliderTime.value = currTime;
        //=============FPS============ UWAGA tutaj nie affected
        fpsTimer += Time.unscaledDeltaTime;
        if (fpsTimer > 0.25f)
        {
            fpsTimer = 0.0f;
            float fps = mainCtrl.GetFPS();
            gfxInfo.fps = (fps > 1000.0f ? 1000.0f : fps);
            RenderGFXInfo();
        }
    }

    private void deactivateAllHelpers()
    {
        HintLineX.gameObject.SetActive(false);
        HintLineY.gameObject.SetActive(false);
        HintLineZ.gameObject.SetActive(false);
        HintLineXY.gameObject.SetActive(false);
        HintLineYX.gameObject.SetActive(false);
        HintLineXZ.gameObject.SetActive(false);
        HintLineZX.gameObject.SetActive(false);
        HintLineYZ.gameObject.SetActive(false);
        HintLineZY.gameObject.SetActive(false);
    }

    void Init()
    {
        meshX = meshY = 1;

        minX = -10.0f;
        maxX = 10.0f;
        minY = -10.0f;
        maxY = 10.0f;
        zScale = 1;
        maxVerts = 50;
        timeStart = 0.0f;
        timeEnd = 0.0f;
        currTime = timeStart;

        UIvisible = true;
        meshColor = MESH_COLOR.HEIGHT_MAP;
        renderMode = RENDER_MODE.SOLID;
        ssao = false;
        gizmo = true;
        doubleSided = true;
        coords = false;

        coordSystem = COORD_SYS.SPHERICAL;

        sphericalNegative = SPH_CYL_NEGATIVE.ABS;

        threads = 1;

        Time.timeScale = 1.0f;
        //meshDensity = 1;

        AssignToUI();

        //skalowanie UI
        bool android = false;
        #if UNITY_ANDROID
        android = true;
        #endif

        /*float scaleUI = Screen.height / panelUI.GetComponent<RectTransform>().sizeDelta.y;
        if (Screen.height >= 768 && !android) scaleUI *= 0.8f;

        canvasUI.GetComponent<Canvas>().scaleFactor = scaleUI;
        canvasUIshow.GetComponent<Canvas>().scaleFactor = scaleUI;
        outputText.fontSize = (int)(outputText.fontSize*scaleUI);
        outputTextInfo.fontSize = (int)(outputTextInfo.fontSize*scaleUI);*/
    }

    void CreateMesh()
    {
        Mesh mesh = null;

        if (coordSystem == COORD_SYS.CARTESIAN)
        {
            mesh = new Mesh();
            mesh.name = "meshModel";

            //how many vertices
            meshX = GetMeshX();
            meshY = GetMeshY();

            CalcMeshSize();

            verts = GetVertices(meshX, meshY);
            mesh.vertices = verts;
            mesh.triangles = GetTris(meshX, meshY);

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            meshObj = new GameObject("Mesh3D", typeof(MeshRenderer), typeof(MeshFilter), typeof(MeshCollider));//TODO TEMP collider
            //HelperClass.error(mesh.bounds.size.z);
            meshObj.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            meshObj.GetComponent<MeshRenderer>().receiveShadows = false;

            meshObj.GetComponent<MeshFilter>().mesh = mesh;
            meshObj.GetComponent<MeshCollider>().sharedMesh = null;

            //meshObj.transform.position = new Vector3(-mesh.bounds.size.x / 2.0f, -mesh.bounds.size.y / 2.0f, -mesh.bounds.size.z / 2.0f);
        }
        else if(coordSystem == COORD_SYS.SPHERICAL)
        {
            //how many vertices
            meshX = GetMeshX();
            meshY = GetMeshY();

            CalcMeshSize();

            CreateSphere(ref meshObj, 1.0f, meshX, meshY);

            meshObj.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            meshObj.GetComponent<MeshRenderer>().receiveShadows = false;

            //ustawianie 'verts'
            mesh = meshObj.GetComponent<MeshFilter>().mesh;
            CalcVertsAndAnglesSPH(mesh);

            //trzeba obrócić ponieważ atan2 zwraca od -PI do PI i musiałem przesunąć zakres o PI i z tyłu zaczyna od 0 stopni...
            //meshObj.transform.Rotate(new Vector3(0.0f, 1.0f, 0.0f), 180.0f);
        }
        else if (coordSystem == COORD_SYS.CYLINDRICAL)
        {
            //how many vertices
            meshX = GetMeshX();
            meshY = GetMeshY();

            CalcMeshSize();

            CreateCylinder(ref meshObj, meshX, meshY);

            meshObj.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            meshObj.GetComponent<MeshRenderer>().receiveShadows = false;

            //ustawianie 'verts'
            mesh = meshObj.GetComponent<MeshFilter>().mesh;
            CalcVertsAndAnglesCYL(mesh);

            //trzeba obrócić ponieważ atan2 zwraca od -PI do PI i musiałem przesunąć zakres o PI i z tyłu zaczyna od 0 stopni...
            //meshObj.transform.Rotate(new Vector3(0.0f, 1.0f, 0.0f), 180.0f);
        }
        meshObj.transform.position = new Vector3(0.0f, 0.0f, 0.0f);

        meshCreated = true;
        meshObj.tag = "mesh3D";

        SetMeshMaterial();

        UpdateMesh();

        if(mesh != null)
            UpdateGFXInfo(mesh.vertices.Length, mesh.triangles.Length / 3);
        RenderGFXInfo();
    }
    void UpdateMesh()//heightmap and color
    {
        //how many vertices
        //int meshX = GetMeshX();
        //int meshY = GetMeshY();

        //zabezpieczenie
        if (verts == null || verts.Length <= 0)
        {
            verts = meshObj.GetComponent<MeshFilter>().mesh.vertices;
            if (coordSystem == COORD_SYS.SPHERICAL)
                for (int i = 0; i < verts.Length; i++)
                    verts[i].Normalize();
            else if (coordSystem == COORD_SYS.CYLINDRICAL)
                for (int i = 0; i < verts.Length; i++)
                {
                    //najpierw normalizujemy wierzchołek, ale tylko X i Z (płaszczyzna XZ)
                    float length = Mathf.Sqrt(verts[i].x * verts[i].x + verts[i].z * verts[i].z);
                    if (length != 0.0f)//nie dziel przez 0
                    {
                        verts[i].x /= length;
                        verts[i].z /= length;
                    }
                }
        }
        //safe
        if (verts != null && verts.Length > 0)
        {
            if (coordSystem == COORD_SYS.CARTESIAN)
            {
                int total = meshX * meshY;
                if (heightMap == null || heightMap.Length != total)
                    heightMap = new float[total];
                //oblicz z
                calcBuffer(currTime);
                //ustaw wierzchołkom heightmapę
                setHeightC(ref verts, meshX, meshY);

                meshObj.GetComponent<MeshFilter>().mesh.vertices = verts;
                //color
                meshObj.GetComponent<MeshFilter>().mesh.colors = GetColorsC(meshX, meshY);
            }
            else if (coordSystem == COORD_SYS.SPHERICAL)
            {
                int total = angles.Length;
                if (angles.Length != verts.Length)
                {
                    HelperClass.error("angles != verts (length)");
                    return;
                }
                if (heightMap == null || heightMap.Length != angles.Length)
                    heightMap = new float[total];

                calcBuffer(currTime);//oblicz radius

                Vector3[] newSphere = new Vector3[total];
                setHeightS(ref newSphere);

                if (meshObj == null)
                {
                    HelperClass.error("null mesh obj");
                    return;
                }
                meshObj.GetComponent<MeshFilter>().mesh.vertices = newSphere;
                //color
                Mesh mesh = meshObj.GetComponent<MeshFilter>().mesh;
                if (mesh == null)
                {
                    HelperClass.error("null mesh");
                    return;
                }
                meshObj.GetComponent<MeshFilter>().mesh.colors = GetColorsS(mesh);
            }
            else if (coordSystem == COORD_SYS.CYLINDRICAL)
            {
                int total = verts.Length;
                if (angles.Length != meshX)
                {
                    HelperClass.error("angles != verts (length)");
                    return;
                }
                if (heightMap == null || heightMap.Length != verts.Length)
                    heightMap = new float[total];

                calcBuffer(currTime);

                Vector3[] newCyl = new Vector3[total];
                setHeightCYL(ref newCyl);

                meshObj.GetComponent<MeshFilter>().mesh.vertices = newCyl;
                //color
                Mesh mesh = meshObj.GetComponent<MeshFilter>().mesh;
                if (mesh == null)
                {
                    HelperClass.error("null mesh");
                    return;
                }
                meshObj.GetComponent<MeshFilter>().mesh.colors = GetColorsCYL(mesh, meshX, meshY);
            }
        }
        else
            HelperClass.error("fatal error! verts null!");

        //HelperClass.log("updating mesh...");
        gfxInfo.meshUpdated = true;
    }
    void ResizeMesh()
    {
        if (verts == null || verts.Length <= 0)
        {
            verts = meshObj.GetComponent<MeshFilter>().mesh.vertices;
            if (coordSystem == COORD_SYS.SPHERICAL)
                for (int i = 0; i < verts.Length; i++)
                    verts[i].Normalize();
            else if (coordSystem == COORD_SYS.CYLINDRICAL)
                for (int i = 0; i < verts.Length; i++)
                {
                    //najpierw normalizujemy wierzchołek, ale tylko X i Z (płaszczyzna XZ)
                    float length = Mathf.Sqrt(verts[i].x * verts[i].x + verts[i].z * verts[i].z);
                    if (length != 0.0f)//nie dziel przez 0
                    {
                        verts[i].x /= length;
                        verts[i].z /= length;
                    }
                }
        }
        //safe
        if (verts != null && verts.Length > 0)
        {
            if (coordSystem == COORD_SYS.CARTESIAN)
            {
                verts = GetVertices(meshX, meshY);
                meshObj.GetComponent<MeshFilter>().mesh.vertices = verts;
            }
            else if(coordSystem == COORD_SYS.SPHERICAL)
            {
                //liczy od nowa verts i kąty
                Mesh mesh = meshObj.GetComponent<MeshFilter>().mesh;
                if (mesh == null)
                {
                    HelperClass.error("null mesh");
                    return;
                }
                CalcVertsAndAnglesSPH(mesh);
            }
            else if (coordSystem == COORD_SYS.CYLINDRICAL)
            {
                //liczy od nowa verts i kąty
                Mesh mesh = meshObj.GetComponent<MeshFilter>().mesh;
                if (mesh == null)
                {
                    HelperClass.error("null mesh");
                    return;
                }
                CalcVertsAndAnglesCYL(mesh);
            }
        }
        else
            HelperClass.error("fatal error! verts null!");
    }
    void RecreateMesh()
    {
        DestroyMesh();
        CreateMesh();
    }
    void DestroyMesh()
    {
        DestroyImmediate(meshObj);
        meshObj = null;
    }
    void OnApplicationQuit()
    {
        DestroyMesh(); 
    }

    void UpdateGFXInfo(int verts, int tris)
    {
        gfxInfo.meshX = meshX;
        gfxInfo.meshY = meshY;
        gfxInfo.verts = verts;
        gfxInfo.tris = tris;
        //gfxInfo.meshDensity = meshDensity;
    }
    void RenderGFXInfo()
    {
        outputTextInfo.text = "###  FPS  =  " + Math.Round(gfxInfo.fps) + "  ###" +
                              "\n###  Time  =  " + Math.Round(currTime) + "  ###" +
                              "\n### MESH INFO ###" +
                              "\nMesh size: " + gfxInfo.meshX + " x " + gfxInfo.meshY +
                              //"\nMesh density: " + gfxInfo.meshDensity +
                              "\nVertices: " + gfxInfo.verts +
                              "\nTriangles: " + gfxInfo.tris +
                              "\nMesh updating: " + gfxInfo.meshUpdated + 
                              "\nZ min: " + gfxInfo.minZ + ", Z max = " + gfxInfo.maxZ +
                              "\n#################" +
                              "\n### ERRORS ###" + 
                              gfxInfo.errors;
    }
    void AddGFXError(String text)
    {
        gfxInfo.errors += "\n" + text;
    }
    
    //TYLKO PRZY INICJACJI BO POWOLNE
    int GetMeshX()
    {
        return 1; 
        /*
         * int x = maxVerts;
        if (x < 1) x = 1;
        return x;*/
        //return Math.Abs(maxX - minX) * meshDensity + 1;
    }
    int GetMeshY()
    {
        return 1;
        /*
        int y = maxVerts;
        if (y < 1) y = 1;
        return y; */
        //return Math.Abs(maxY - minY)* meshDensity + 1;
    }

    void CalcMeshSize()
    {
        //maksymalizuje jakość a potem
        //obniża jakość jeśli nie można tyle vertices
        /*meshDensity = 1;
        while ( GetMeshX() * GetMeshY() <= maxVerts)
        {
            meshDensity++;
        } 
        //now it's too much:
        meshDensity--;

        //total vertices will be meshX * meshY, it can't exceed 65 000 because of unity....
        while (GetMeshX() * GetMeshY() > 65000)
        {
            if (meshDensity <= 1)
            {
                AddGFXError("Too much vertices, can't reduce quality.");//Max 65000. Reducing quality...");
                break;
            }
            else
                meshDensity--;
        }

        if (meshDensity < 1)
        {
            meshDensity = 1;
            AddGFXError("'Max vertices' is too little.Ignoring...");
        }*/

        if (coordSystem == COORD_SYS.CARTESIAN)
        {
            //'dopycha' aby było więcej jeśli się da
            while ((meshX + 1) * (meshY + 1) <= maxVerts*maxVerts)
            {
                meshX += 1;
                meshY += 1;
            }
        }
        else if (coordSystem == COORD_SYS.SPHERICAL)
        {
            //verts = (meshX+1)*meshY + 2 (nie wiem czemu meshX+1 zamiast meshX - pewnie źle skleja
            while ((meshX + 2) * (meshY+1) + 2 <= maxVerts*maxVerts)
            {
                meshX += 2;//because more vertices is needed in this 'axis'
                meshY += 1;
            }
        }
        else if (coordSystem == COORD_SYS.CYLINDRICAL)
        {
            //verts = (meshX+1)*meshY + 2 (nie wiem czemu meshX+1 zamiast meshX - pewnie źle skleja
            while ((meshX + 1) * (meshY + 1) <= maxVerts * maxVerts)
            {
                meshX += 1;//because more vertices is needed in this 'axis'
                meshY += 1;
            }
        }

    }
    //for cartesian
    Vector3[] GetVertices(int w, int h)
    {
        Vector3[] vertices;
        int total = 1;

        total = w * h;

        vertices = new Vector3[total];

        float x0 = -20f;
        float y0 = -20f;

        float dx = 2 * (-x0) / (meshX - 1);
        float dy = 2 * (-y0) / (meshY - 1);

        //float oX = (float)(maxX - minX) / (meshX-1);
        //float oY = (float)(maxY - minY) / (meshY-1);
        for (int i = 0; i < h; i++)
            for (int j = 0; j < w; j++)
            {
                vertices[i * w + j] = new Vector3(x0 + j * dx, 0.0f, y0 + i * dy);
            }
        return vertices;
    }
    int[] GetTris(int w, int h)
    {
        int[] tris;
        int total = (w - 1) * 6 * (h - 1);
        tris = new int[total];

        for (int i = 0; i < h - 1; i++)
            for (int j = 0; j < w - 1; j++)
            {
                //HelperClass.log("Curr triangle: " + (i * (w-1) + j*6).ToString());
                tris[(i * (w - 1) + j) * 6] = j + i * w;
                tris[(i * (w - 1) + j) * 6 + 1] = j + (i + 1) * w;
                tris[(i * (w - 1) + j) * 6 + 2] = j + i * w + 1;

                tris[(i * (w - 1) + j) * 6 + 3] = j + i * w + 1;
                tris[(i * (w - 1) + j) * 6 + 4] = j + (i + 1) * w;
                tris[(i * (w - 1) + j) * 6 + 5] = j + (i + 1) * w + 1;
            }

        //HelperClass.log("Tris: " + total.ToString());

        /*for(int i=0;i<total;i++)
            HelperClass.log(tris[i].ToString());*/
        return tris;
    }
    void setHeightC(ref Vector3[] vertices, int w, int h)//cartesian coord
    {
        if (coordSystem != COORD_SYS.CARTESIAN)
            return;

        if (heightMap.Length > vertices.Length || w * h > heightMap.Length || w * h > vertices.Length)
            return;

        for (int i = 0; i < h; i++)
            for (int j = 0; j < w; j++)
            {
                //ZABEZPIECZENIE PRZED NaN
                if (float.IsNaN(heightMap[i * w + j]) || float.IsInfinity(heightMap[i * w + j]))
                    heightMap[i * w + j] = 0.0f;
                vertices[i * w + j].y = heightMap[i * w + j];//zScale * heightMap[i * w + j];
            }
        //calculate vertex normals to smooth mesh TODO
    }
    Color[] GetColorsC(int w, int h)
    {
        Color[] colors = new Color[w * h];

        //zabezpieczenie
        if (coordSystem != COORD_SYS.CARTESIAN ||
            w * h > heightMap.Length)
        {
            for (int i = 0; i < heightMap.Length; i++)
                colors[i] = new Color(1.0f, 1.0f, 1.0f);
            return colors;
        }

        float min = (mev != null ? mev.Min : 0.0f);
        float max = (mev != null ? mev.Max : 1.0f);

        gfxInfo.minZ = min;
        gfxInfo.maxZ = max;

        //no height, one color
        if (min == max)
        {
            for (int i = 0; i < h; i++)
                for (int j = 0; j < w; j++)
                {
                    colors[i * w + j] = new Color(1, 1, 1);
                }
        }

        for (int i = 0; i < h; i++)
            for (int j = 0; j < w; j++)
            {
                float r, g, b;

                switch (meshColor)
                {
                    case MESH_COLOR.SINGLE_WHITE:
                    default:
                        colors[i * w + j] = new Color(1, 1, 1);
                        break;
                    case MESH_COLOR.SINGLE:
                        colors[i * w + j] = new Color(0, 1, 0);
                        break;
                    case MESH_COLOR.HEIGHT:
                        b = (heightMap[i * w + j] - min) / (max - min);
                        r = 0.0f;
                        colors[i * w + j] = new Color(r, r, b);
                        break;
                    case MESH_COLOR.HEIGHT_WHITE:
                        r = (heightMap[i * w + j] - min) / (max - min);
                        colors[i * w + j] = new Color(r, r, r);
                        break;
                    case MESH_COLOR.HEIGHT_MAP:
                        {
                            float x = (heightMap[i * w + j] - min) / (max - min);
                            int ix = (int)(x * COLOR_RES);
                            if (ix == COLOR_RES) ix--;
                            if (ix < 0) ix = 0; else if (ix >= colorMap.Length) ix = colorMap.Length - 1;
                            colors[i * w + j] = colorMap[ix];
                        }
                        break;
                    case MESH_COLOR.FANCY:
                        r = (heightMap[i * w + j] - min) / (max - min);
                        g = (float)i / h;
                        b = (float)j / w;
                        colors[i * w + j] = new Color(r, g, b);
                        break;
                }
            }

        return colors;
    }
    //for spherical
    void CalcVertsAndAnglesSPH(Mesh mesh)
    {
        int v = mesh.vertices.Length;

        verts = new Vector3[v];
        angles = new Vector2[v];

        verts = mesh.vertices;

        for (int i = 0; i < v; i++)
        {
            verts[i].Normalize();
            //bierzemy kąt na pł. XZ i na pł przecinającej XZ
            //najpierw normalizujemy wierzchołek
            float a, b;//kąty
            a = Mathf.Atan2(verts[i].z, verts[i].x) + Mathf.PI;
            b = Mathf.Acos(verts[i].y);
            angles[i] = new Vector2(a, b);
        }
    }
    void setHeightS(ref Vector3[] vertices)//spherical coord
    {
        if (coordSystem != COORD_SYS.SPHERICAL)
            return;

        if (heightMap.Length > vertices.Length || verts.Length != vertices.Length)
        {
            HelperClass.error("wrong length");
            return;
        }

        for (int i = 0; i < heightMap.Length; i++)
        {
            //ZABEZPIECZENIE PRZED NaN
            if (float.IsNaN(heightMap[i]) || float.IsInfinity(heightMap[i]))
                heightMap[i] = 1.0f;

            vertices[i] = heightMap[i] * verts[i];// *zScale;
        }
    }
    Color[] GetColorsS(Mesh mesh)
    {
        int total = mesh.vertices.Length;
        Color[] colors = new Color[total];

        //zabezpieczenie
        if (coordSystem != COORD_SYS.SPHERICAL ||
            total > heightMap.Length || mev == null)
        {
            for (int i = 0; i < heightMap.Length; i++)
                colors[i] = new Color(1.0f, 1.0f, 1.0f);
            return colors;
        }

        float min = mev.Min;
        float max = mev.Max;

        gfxInfo.minZ = min;
        gfxInfo.maxZ = max;

        //no height, one color
        if (min == max)
        {
            for (int i = 0; i < total; i++)
                {
                    float r = 1.0f, g = 1.0f, b = 1.0f;
                    colors[i] = new Color(r, g, b);
                }
        }

        for (int i = 0; i < total; i++)
        {
            float r, g, b;

            switch (meshColor)
            {
                case MESH_COLOR.SINGLE_WHITE:
                default:
                    colors[i] = new Color(1, 1, 1);
                    break;
                case MESH_COLOR.SINGLE:
                    colors[i] = new Color(0, 1, 0);
                    break;
                case MESH_COLOR.HEIGHT:
                    b = (heightMap[i] - min) / (max - min);
                    r = 0.0f;
                    colors[i] = new Color(r, r, b);
                    break;
                case MESH_COLOR.HEIGHT_WHITE:
                    r = (heightMap[i] - min) / (max - min);
                    colors[i] = new Color(r, r, r);
                    break;
                case MESH_COLOR.HEIGHT_MAP:
                    {
                        float x = (heightMap[i] - min) / (max - min);
                        int ix = (int)(x * COLOR_RES);
                        if (ix == COLOR_RES) ix--;
                        if (ix < 0) ix = 0; else if (ix >= colorMap.Length) ix = colorMap.Length - 1;
                        colors[i] = colorMap[ix];
                    }
                    break;
                case MESH_COLOR.FANCY:
                    r = HelperClass.Clamp(Mathf.Sin(0.7f * (heightMap[i] - min) / (max - min)), 0.0f, 1.0f);
                    g = HelperClass.Clamp(Mathf.Sin(i / 20.0f), 0.0f, 1.0f);
                    b = HelperClass.Clamp(Mathf.Cos(i / 9.0f), 0.0f, 1.0f);
                    colors[i] = new Color(r, g, b);
                    break;
            }

        }

        return colors;
    }
    //for cylindrical
    void CalcVertsAndAnglesCYL(Mesh mesh)
    {
        //UŻYWA TYLKO PIERWSZEJ WSPÓŁRZ. W ANGLES
        int v = mesh.vertices.Length;
        if (meshX <= 0)
        {
            HelperClass.error("meshX <= 0");
            meshX = 1;
        }

        verts = new Vector3[v];
        angles = new Vector2[meshX];

        verts = mesh.vertices;

        float dz = (maxX - minX) / (meshY-1);

        for (int i = 0; i < v; i++)
        {
            //najpierw "normalizujemy" wierzchołek, ale tylko dla X i Z (płaszczyzna XZ)
            float length = Mathf.Sqrt(verts[i].x*verts[i].x + verts[i].z*verts[i].z);
            if (length != 0.0f)//nie dziel przez 0
            {
                verts[i].x /= length;
                verts[i].z /= length;
            }
            verts[i].y = minX + (meshY - 1 - (int)i/(int)meshX) * dz;
        }
        for (int i = 0; i < meshX; i++)
        {
            //kąty
            //bierzemy kąt na pł. XZ
            float a;//kąty
            a = Mathf.Atan2(verts[i].z, verts[i].x) + Mathf.PI;
            angles[i] = new Vector2(a, 0.0f);
        }
    }
    void setHeightCYL(ref Vector3[] vertices)//cylindrical coord
    {
        if (coordSystem != COORD_SYS.CYLINDRICAL)
            return;

        if (heightMap.Length > vertices.Length || verts.Length != vertices.Length)
        {
            HelperClass.error("wrong length");
            return;
        }

        for (int i = 0; i < heightMap.Length; i++)
        {
            //ZABEZPIECZENIE PRZED NaN
            if (float.IsNaN(heightMap[i]) || float.IsInfinity(heightMap[i]))
                heightMap[i] = 1.0f;

            vertices[i].x = heightMap[i] * verts[i].x;// *zScale;
            vertices[i].z = heightMap[i] * verts[i].z;// *zScale;
            vertices[i].y = verts[i].y;
        }
    }
    Color[] GetColorsCYL(Mesh mesh, int w, int h)
    {
        int total = mesh.vertices.Length;
        Color[] colors = new Color[total];

        //zabezpieczenie
        if (coordSystem != COORD_SYS.CYLINDRICAL ||
            total > heightMap.Length || mev == null ||
            w * h > heightMap.Length)
        {
            for (int i = 0; i < heightMap.Length; i++)
                colors[i] = new Color(1.0f, 1.0f, 1.0f);
            return colors;
        }

        float min = mev.Min;
        float max = mev.Max;

        gfxInfo.minZ = min;
        gfxInfo.maxZ = max;

        //no height, one color
        if (min == max)
        {
            for (int i = 0; i < total; i++)
            {
                float r = 1.0f, g = 1.0f, b = 1.0f;
                colors[i] = new Color(r, g, b);
            }
        }

        for (int i = 0; i < h; i++)
            for (int j = 0; j < w; j++)
            {
                float r, g, b;

                switch (meshColor)
                {
                    case MESH_COLOR.SINGLE_WHITE:
                    default:
                        colors[i * w + j] = new Color(1, 1, 1);
                        break;
                    case MESH_COLOR.SINGLE:
                        colors[i * w + j] = new Color(0, 1, 0);
                        break;
                    case MESH_COLOR.HEIGHT:
                        b = (heightMap[i * w + j] - min) / (max - min);
                        r = 0.0f;
                        colors[i * w + j] = new Color(r, r, b);
                        break;
                    case MESH_COLOR.HEIGHT_WHITE:
                        r = (heightMap[i * w + j] - min) / (max - min);
                        colors[i * w + j] = new Color(r, r, r);
                        break;
                    case MESH_COLOR.HEIGHT_MAP:
                        {
                            float x = (heightMap[i * w + j] - min) / (max - min);
                            int ix = (int)(x * COLOR_RES);
                            if (ix == COLOR_RES) ix--;
                            if (ix < 0) ix = 0; else if (ix >= colorMap.Length) ix = colorMap.Length - 1;
                            colors[i * w + j] = colorMap[ix];
                        }
                        break;
                    case MESH_COLOR.FANCY:
                        r = (heightMap[i * w + j] - min) / (max - min);
                        g = (float)i / h;
                        b = (float)j / w;
                        colors[i * w + j] = new Color(r, g, b);
                        break;
                }
            }

        return colors;
    }

    /*float function3DT(float x, float y, float t)  //debug
    {
        return x * x;//Mathf.Sin(x * x - Mathf.Pow((Mathf.Sin(t))*4, 2));// (Mathf.Sin((t % 1) * 10.0f + x) + Mathf.Cos((t % 1) * 10.0f + y));
    }
    void draw3DTFunction(ref float[] heightMap, int w, int h)
    {
        for (int i = 0; i < h; i++)
            for (int j = 0; j < w; j++)
                heightMap[i * w + j] = function3DT(j, i, runTime);
    }*/

    /// <summary>
    /// Parses the function specified in InputFunc.
    /// Called by button click event.
    /// </summary>
    public void ParseFunction()
    {
        if (!meshCreated)
            return;

        currTime = timeStart;

        parser = new MathParser();
        try
        {
            parser.ReadPostfix(InputFunc.GetComponent<InputField>().text);
            mev = new MathEval(parser, new Dictionary<int, double>(), meshX * meshX);
            outputText.text = "";     //was gud, set to null :>
        }
        catch
        {
            try
            {
                parser.ReadInfix(InputFunc.GetComponent<InputField>().text);
                mev = new MathEval(parser, new Dictionary<int, double>(), meshX * meshX);
                outputText.text = "";     //was gud, set to null :>
            }
            catch (Exception ex)
            {
                outputText.text = ex.Message;
            }
        }
        

        UpdateMesh();
    }

    /// <summary>
    /// Calculates the specified heightmap using current range values and passed t argument.
    /// </summary>
    /// <param name="buffer">The buffer.</param>
    /// <param name="t">The t.</param>
    private void calcBuffer(float t)
    {
        if (mev != null && mev.CalcStack.Count > 0)
        {
            if (coordSystem == COORD_SYS.CARTESIAN)
            {
                double dx = (maxX - minX) / (double)(meshX - 1);    //maxX > minX
                double dy = (maxY - minY) / (double)(meshY - 1);

                mev.EvaluateMatrix(ref heightMap, minX, dx, minY, dy, meshX, meshY, t);
            }
            else if (coordSystem == COORD_SYS.SPHERICAL)
            {
                if (verts.Length > angles.Length)
                {
                    HelperClass.error("fatal error! more angles than vertices!");
                    return;
                }
                mev.EvaluateMatrix(ref heightMap, ref angles, t, sphericalNegative);
            }
            else if (coordSystem == COORD_SYS.CYLINDRICAL)
            {
                if (meshX != angles.Length)
                {
                    HelperClass.error("error! more angles != meshX!");
                    return;
                }
                double dx = 2.0 * Math.PI / meshX;
                double dy = (maxX - minX) / (double)(meshY - 1);
                mev.EvaluateMatrix(ref heightMap, 0, dx, minX, dy, meshX, meshY, t, sphericalNegative);
            }
        }
    }

    void AssignToUI()
    {
        //outputTextInfo.text = "";
        InputMinX.text = minX.ToString();
        InputMaxX.text = maxX.ToString();
        InputMinY.text = minY.ToString();
        InputMaxY.text = maxY.ToString();
        InputMinX.interactable = (coordSystem != COORD_SYS.SPHERICAL ? true : false);
        InputMaxX.interactable = (coordSystem != COORD_SYS.SPHERICAL ? true : false);
        InputMinY.interactable = (coordSystem == COORD_SYS.CARTESIAN ? true : false);
        InputMaxY.interactable = (coordSystem == COORD_SYS.CARTESIAN ? true : false);
        if (coordSystem == COORD_SYS.CYLINDRICAL)
        {
            descMaxX.text = "Max Z";
            descMinX.text = "Min Z";
        }
        else
        {
            descMaxX.text = "Max X";
            descMinX.text = "Min X";
        }
        
        InputScaleZ.text = zScale.ToString();
        InputMaxVerts.text = maxVerts.ToString();
        InputTimeStart.text = timeStart.ToString();
        InputTimeEnd.text = timeEnd.ToString();
        InputThreads.text = threads.ToString();
        InputFunc.text = "14cos(tg(sinx)-yt)";// "2(0 max (-sin(x/3+t)cos(x/2) - cos(y/3+t)sin(x/2)))";

        pointCoordText.GetComponent<Text>().text = "";
        pointCoord.SetActive(false);

        Camera.main.GetComponent<ScreenSpaceAmbientObscurance>().enabled = ssao;
        ToggleUI.isOn = UIvisible;
        canvasUI.SetActive(UIvisible);
        sliderTime.minValue = timeStart;
        sliderTime.maxValue = timeEnd;
        sliderTime.value = currTime;
        ToggleGizmo.isOn = gizmo;
        Gizmo.SetActive(gizmo);
        ToggleDouble.isOn = doubleSided;
        TogglePause.isOn = false;
        ToggleCoords.isOn = coords;
        ToggleCoords.interactable = TogglePause.isOn;
        pointCoord.SetActive(false);
        dropdownColor.value = (int)meshColor;
        dropdownColor.interactable = (renderMode == RENDER_MODE.WIREFRAME ? false : true);
        dropdownRendering.value = (int)renderMode;
        dropdownCoordSystem.value = (int)coordSystem;
        dropdownSphericalNegative.value = (int)sphericalNegative;
        dropdownSphericalNegative.interactable = ((coordSystem == COORD_SYS.SPHERICAL || coordSystem == COORD_SYS.CYLINDRICAL) ? true : false);
    }
    void ParseInt(String text, ref int value, int Default = 10)//parses or ...
    {
        //default 1
        int Out = 1;
        if (int.TryParse(text, out Out)) value = Out;
        else value = Default;
    }
    void ParseFloat(String text, ref float value, float Default = 10.0f)//parses or ...
    {
        //default 1
        float Out = 1;
        if (float.TryParse(text, out Out)) value = Out;
        else value = Default;
    }
    void SetInputField(InputField field, String text)
    {
        field.text = text;
    }


    private void SetMeshMaterial()
    {
        if (!meshCreated)
            return;

        if (meshObj != null && meshObj.GetComponent<MeshRenderer>())
            switch (renderMode)
            {
                case RENDER_MODE.SOLID:
                    meshObj.GetComponent<MeshRenderer>().material = (!doubleSided ? meshMaterialOneSided : meshMaterialTwoSided);

                    break;
                case RENDER_MODE.WIREFRAME:
                    meshObj.GetComponent<MeshRenderer>().material = (!doubleSided ? meshMaterialWireOne : meshMaterialWireTwo);
                    break;
                default:
                    break;
            }
    }
    private bool IsTimeStopped()
    {
        //fucking float....
        return (Time.timeScale <= 0.01f);
    }
    void CreateSphere(ref GameObject sphere, float radius, int nbLong, int nbLat)
    {
        if (nbLong < 1) nbLong = 1;
        if (nbLat < 1) nbLat = 1;
        sphere = new GameObject("Mesh3D", typeof(MeshRenderer), typeof(MeshFilter), typeof(MeshCollider));//, typeof(MeshCollider));
        MeshFilter filter = sphere.GetComponent<MeshFilter>();
        sphere.GetComponent<MeshCollider>().sharedMesh = null;
        Mesh mesh = filter.mesh;

        mesh.Clear();

        // Longitude |||
        //int nbLong = 24;
        // Latitude ---
        //int nbLat = 16;

        #region Vertices
        Vector3[] vertices = new Vector3[(nbLong + 1) * nbLat + 2];
        float _pi = Mathf.PI;
        float _2pi = _pi * 2f;

        vertices[0] = Vector3.up * radius;
        for (int lat = 0; lat < nbLat; lat++)
        {
            float a1 = _pi * (float)(lat + 1) / (nbLat + 1);
            float sin1 = Mathf.Sin(a1);
            float cos1 = Mathf.Cos(a1);

            for (int lon = 0; lon <= nbLong; lon++)
            {
                float a2 = _2pi * (float)(lon == nbLong ? 0 : lon) / nbLong;
                float sin2 = Mathf.Sin(a2);
                float cos2 = Mathf.Cos(a2);

                vertices[lon + lat * (nbLong + 1) + 1] = new Vector3(sin1 * cos2, cos1, sin1 * sin2) * radius;
            }
        }
        vertices[vertices.Length - 1] = Vector3.up * -radius;
        #endregion

        #region Normales
        Vector3[] normales = new Vector3[vertices.Length];
        for (int n = 0; n < vertices.Length; n++)
            normales[n] = vertices[n].normalized;
        #endregion

        #region UVs
        Vector2[] uvs = new Vector2[vertices.Length];
        uvs[0] = Vector2.up;
        uvs[uvs.Length - 1] = Vector2.zero;
        for (int lat = 0; lat < nbLat; lat++)
            for (int lon = 0; lon <= nbLong; lon++)
                uvs[lon + lat * (nbLong + 1) + 1] = new Vector2((float)lon / nbLong, 1f - (float)(lat + 1) / (nbLat + 1));
        #endregion

        #region Triangles
        int nbFaces = vertices.Length;
        int nbTriangles = nbFaces * 2;
        int nbIndexes = nbTriangles * 3;
        int[] triangles = new int[nbIndexes];

        //Top Cap
        int i = 0;
        for (int lon = 0; lon < nbLong; lon++)
        {
            triangles[i++] = lon + 2;
            triangles[i++] = lon + 1;
            triangles[i++] = 0;
        }

        //Middle
        for (int lat = 0; lat < nbLat - 1; lat++)
        {
            for (int lon = 0; lon < nbLong; lon++)
            {
                int current = lon + lat * (nbLong + 1) + 1;
                int next = current + nbLong + 1;

                triangles[i++] = current;
                triangles[i++] = current + 1;
                triangles[i++] = next + 1;

                triangles[i++] = current;
                triangles[i++] = next + 1;
                triangles[i++] = next;
            }
        }

        //Bottom Cap
        for (int lon = 0; lon < nbLong; lon++)
        {
            triangles[i++] = vertices.Length - 1;
            triangles[i++] = vertices.Length - (lon + 2) - 1;
            triangles[i++] = vertices.Length - (lon + 1) - 1;
        }
        #endregion

        mesh.vertices = vertices;
        mesh.normals = normales;
        mesh.uv = uvs;
        mesh.triangles = triangles;
        mesh.name = "meshModel";

        mesh.RecalculateBounds();
    }
    void CreateCylinder(ref GameObject cylinder, int x, int y)
    {
        //how many vertices
        //meshX = GetMeshX();
        //meshY = GetMeshY();

        //CalcMeshSize();
        if (x < 3) x = 3;
        if (y < 2) y = 2;

        cylinder = new GameObject("Mesh3D", typeof(MeshRenderer), typeof(MeshFilter), typeof(MeshCollider));//, typeof(MeshCollider));
        MeshFilter filter = cylinder.GetComponent<MeshFilter>();
        cylinder.GetComponent<MeshCollider>().sharedMesh = null;
        Mesh mesh = filter.mesh;

        mesh.Clear();

        //VERTICES
        //cylinder będzie miał quadów w poziomie x, a w pionie y-1...
        Vector3[] vertices;
        int total = x * y;

        vertices = new Vector3[total];

        float alfa = 2 * Mathf.PI / x;
        float dy = (maxX - minX) / (y - 1);

        //verteksów tyle co (y+1) * x
        for (int i = 0; i < y; i++)
            for (int j = 0; j < x; j++)
            {
                vertices[i * x + j] = new Vector3(Mathf.Cos(alfa*j), minX + (y - 1 - i) * dy, Mathf.Sin(alfa*j));
            }
        mesh.vertices = vertices;

        //TRIANGLES
        /*int[] tris;
        total = x * (y - 1) * 6;//każdy quad to 2 trójkąty po 3 wierzchołki
        tris = new int[total];

        for (int i = 0; i < y - 1; i++)
            for (int j = 0; j < x; j++)
            {
                //HelperClass.log("Curr triangle: " + (i * (w-1) + j*6).ToString());
                tris[(i * x + j) * 6] = j + i * (x - 1);
                tris[(i * x + j) * 6 + 1] = j + (i + 1) * (x - 1);
                //if(j == x-1)
                tris[(i * x + j) * 6 + 2] = j + i * (x - 1) + 1;

                tris[(i * x + j) * 6 + 3] = j + i * (x - 1) + 1;
                tris[(i * x + j) * 6 + 4] = j + (i + 1) * (x - 1);
                tris[(i * x + j) * 6 + 5] = j + (i + 1) * (x - 1) + 1;
            }
        mesh.triangles = tris;
        */
        int[] tris;
        total = x * 6 * (y - 1);
        tris = new int[total];

        for (int i = 0; i < y - 1; i++)
            for (int j = 0; j < x; j++)
            {
                //UWAGA tutaj pozamieniana kolejność wierzchołków żeby normalne były z drugiej strony bo były odwrócone
                //HelperClass.log("Curr triangle: " + (i * (x-1) + j*6).ToString());
                tris[(i * x + j) * 6] = j + i * x;
                tris[(i * x + j) * 6 + 2] = j + (i + 1) * x;
                //ostatni w wierszu to pierwszy wierzchołek z tego wiersza bierze, pętla bo to cylinder
                tris[(i * x + j) * 6 + 1] = (j % (x - 1)) + i * x + (j == x - 1 ? 0 : 1);

                tris[(i * x + j) * 6 + 3] = (j % (x - 1)) + i * x + (j == x - 1 ? 0 : 1);
                tris[(i * x + j) * 6 + 5] = j + (i + 1) * x;
                //ostatni w wierszu to pierwszy wierzchołek z tego wiersza bierze, pętla bo to cylinder
                tris[(i * x + j) * 6 + 4] = (j % (x - 1)) + (i + 1) * x + (j == x-1 ? 0 : 1);
            }
        mesh.triangles = tris;

        //NORMALS
        /*Vector3[] normales = new Vector3[vertices.Length];
        for (int n = 0; n < vertices.Length; n++)
            normales[n] = vertices[n].normalized;

        mesh.normals = normales;*/

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        mesh.name = "meshModel";

        cylinder.GetComponent<MeshFilter>().mesh = mesh;
        cylinder.GetComponent<MeshCollider>().sharedMesh = null;
        //cylinder.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
    }

    public Vector3 getPlotBoundsMin()
    {
        return new Vector3(minX, minY, gfxInfo.minZ);
    }
    public Vector3 getPlotBoundsMax()
    {
        return new Vector3(maxX, maxY, gfxInfo.maxZ);
    }
}