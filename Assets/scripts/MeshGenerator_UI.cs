﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using MathEngine;
using System;
using UnityStandardAssets.ImageEffects;
using System.Threading;
using System.Collections.Generic;

public partial class MeshGenerator : MonoBehaviour
{
    public Text outputText, outputTextInfo;
    public InputField InputMinX, InputMaxX, InputMinY, InputMaxY, InputScaleZ, InputMaxVerts, InputTimeStart, InputTimeEnd;
    public Text descMinX, descMaxX;
    public InputField InputFunc, InputThreads;
    public Toggle ToggleGizmo, ToggleDouble, TogglePause, ToggleCoords;
    public Slider sliderTime;
    public Dropdown dropdownColor, dropdownRendering, dropdownCoordSystem, dropdownSphericalNegative;
    public Toggle ToggleUI;

    public void inputUpdate()
    {
        //ponieważ przy starcie aplikacji unity wywołuje inputUpdate dla każdego obiektu UI....
        if (!meshCreated)
            return;

        ParseFloat(InputMinX.text, ref minX);
        if (minX >= maxX)
        {
            maxX = minX + 1.0f;
            SetInputField(InputMaxX, maxX.ToString());

        }
        ParseFloat(InputMaxX.text, ref maxX);
        if (minX >= maxX)
        {
            minX = maxX - 1.0f;
            SetInputField(InputMinX, minX.ToString());
        }
        ParseFloat(InputMinY.text, ref minY);
        if (minY >= maxY)
        {
            maxY = minY + 1.0f;
            SetInputField(InputMaxY, maxY.ToString());
        }
        ParseFloat(InputMaxY.text, ref maxY);
        if (minY >= maxY)
        {
            minY = maxY - 1.0f;
            SetInputField(InputMinY, minY.ToString());
        }

        //if (timeStart == timeEnd && IsTimeStopped())
        //    currTime = timeStart;
        ResizeMesh();
        UpdateMesh();
    }
    public void inputUpdatNewMesh()
    {
        //ponieważ przy starcie aplikacji unity wywołuje inputUpdate dla każdego obiektu UI....
        if (!meshCreated)
            return;

        ParseInt(InputMaxVerts.text, ref maxVerts);
        if (maxVerts < 4)
        {
            maxVerts = 4;
            SetInputField(InputMaxVerts, maxVerts.ToString());
        }
        else if (maxVerts > 254)//ograniczenie unity
        {
            maxVerts = 254;
            SetInputField(InputMaxVerts, maxVerts.ToString());
        }
        try
        {
            parser.ReadPostfix(InputFunc.GetComponent<InputField>().text);
            mev = new MathEval(parser, new Dictionary<int, double>(), meshX * meshX);
            outputText.text = "";     //was gud, set to null :>
        }
        catch
        {
            try
            {
                parser.ReadInfix(InputFunc.GetComponent<InputField>().text);
                mev = new MathEval(parser, new Dictionary<int, double>(), meshX * meshX);
                outputText.text = "";     //was gud, set to null :>
            }
            catch (Exception ex)
            {
                outputText.text = ex.Message;
            }
        }

        coordSystem = (COORD_SYS)dropdownCoordSystem.value;
        dropdownSphericalNegative.interactable = ((coordSystem == COORD_SYS.SPHERICAL || coordSystem == COORD_SYS.CYLINDRICAL) ? true : false);

        InputMinX.interactable = (coordSystem != COORD_SYS.SPHERICAL ? true : false);
        InputMaxX.interactable = (coordSystem != COORD_SYS.SPHERICAL ? true : false);
        InputMinY.interactable = (coordSystem == COORD_SYS.CARTESIAN ? true : false);
        InputMaxY.interactable = (coordSystem == COORD_SYS.CARTESIAN ? true : false);
        if (coordSystem == COORD_SYS.CYLINDRICAL)
        {
            descMaxX.text = "Max Z";
            descMinX.text = "Min Z";
        }
        else
        {
            descMaxX.text = "Max X";
            descMinX.text = "Min X";
        }

        RecreateMesh();
    }
    public void inputScaleZ()
    {
        if (!meshCreated)
            return;

        ParseFloat(InputScaleZ.text, ref zScale);
        if (zScale == 0 || float.IsNaN(zScale) || float.IsInfinity(zScale))
        {
            zScale = 1;
        }

        if (coordSystem == COORD_SYS.CARTESIAN)
            ResizeMesh();
        else if (coordSystem == COORD_SYS.SPHERICAL)
        { }
        else if (coordSystem == COORD_SYS.CYLINDRICAL)
        { }

        UpdateMesh();
    }
    public void inputSphericalNegative()
    {
        if (!meshCreated)
            return;

        sphericalNegative = (SPH_CYL_NEGATIVE)dropdownSphericalNegative.value;

        UpdateMesh();
    }

    public void changeColor()
    {
        if (!meshCreated)
            return;

        meshColor = (MESH_COLOR)dropdownColor.value;

        UpdateMesh();
    }
    public void changeMode()//changes rendering mode
    {
        if (!meshCreated)
            return;

        renderMode = (RENDER_MODE)dropdownRendering.value;
        if (renderMode == RENDER_MODE.WIREFRAME)
            dropdownColor.interactable = false;
        else dropdownColor.interactable = true;

        SetMeshMaterial();
    }
    public void toggleUI()
    {
        if (!meshCreated)
            return;

        UIvisible = !UIvisible;
        ToggleUI.isOn = UIvisible;
        canvasUI.SetActive(UIvisible);
    }
    public void toggleDoubleSided()
    {
        if (!meshCreated)
            return;

        doubleSided = !doubleSided;
        ToggleDouble.isOn = doubleSided;

        RecreateMesh();
    }
    public void toggleGizmo()
    {
        if (!meshCreated)
            return;

        gizmo = !gizmo;
        ToggleGizmo.isOn = gizmo;
        Gizmo.SetActive(gizmo);
    }
    public void togglePause()
    {
        if (!meshCreated)
            return;

        if (IsTimeStopped())//bo float jest ... weird
        {
            Time.timeScale = 1.0f;
            ToggleCoords.isOn = false;
            ToggleCoords.interactable = false;
            //ToggleCoords.enabled = false;
            coords = false;
            if (pointCoordText.GetComponent<Text>().text != "")
            {
                pointCoordText.GetComponent<Text>().text = "";
                pointCoord.SetActive(false);
            }
        }
        else
        {
            Time.timeScale = 0.0f;
            ToggleCoords.interactable = true;
            //ToggleCoords.enabled = true;
            ToggleCoords.isOn = true;
        }
    }
    public void toggleCoords()//coordinate box (mouse point screen raycast...)
    {
        if (!meshCreated)
            return;

        if (IsTimeStopped())
        {
            coords = !coords;
            if (!coords)
            {
                meshObj.GetComponent<MeshCollider>().sharedMesh = null;
                pointCoord.SetActive(false);
            }
        }
        else
        {
            ToggleCoords.isOn = false;
            coords = false;
        }
    }
    public void changeTime()
    {
        ParseFloat(InputTimeStart.text, ref timeStart);
        if (timeStart > timeEnd)
        {
            timeEnd = timeStart + 1;
            SetInputField(InputTimeEnd, timeEnd.ToString());
        }
        ParseFloat(InputTimeEnd.text, ref timeEnd);
        if (timeStart > timeEnd)
        {
            timeStart = timeEnd - 1;
            SetInputField(InputTimeStart, timeStart.ToString());
        }

        sliderTime.minValue = timeStart;
        sliderTime.maxValue = timeEnd;

        currTime = sliderTime.value;

        if (mev.ContainsT && IsTimeStopped())
            UpdateMesh();
    }
    public void changeThreads()
    {
        ParseInt(InputThreads.text, ref threads);
        if (threads >= 32)
        {
            threads = 32;
            SetInputField(InputThreads, threads.ToString());
        }
        else if (threads < 1)
        {
            threads = 1;
            SetInputField(InputThreads, threads.ToString());
        }
    }

}

