﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class ThreadInfo
{
    public int StartFrom;
    public int EndBefore;
    public int MeshX;
    public int MeshY;
    public float T;

    public ThreadInfo(int startFrom, int endBefore, int meshX, int meshY, float t)
    {
        StartFrom = startFrom;
        EndBefore = endBefore;
        MeshX = meshX;
        MeshY = meshY;
        T = t;
    }
}
