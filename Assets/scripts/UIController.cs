﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UIController : MonoBehaviour {
    public GameObject tabFun;
    public GameObject tabColor;
    public GameObject tabGrid;

    private void Start()
    {
        SetFun();
    }

    public void SetFun()
    {
        setActives(001);
    }

    public void SetColor()
    {
        setActives(010);
    }

    public void SetGrid()
    {
        setActives(100);
    }

    private void setActives(int x)
    {
        tabFun.SetActive(x % 10 != 0);
        tabColor.SetActive(x % 100 > 9);
        tabGrid.SetActive(x % 1000 > 99);
    }
}
